#include "mt.h"

/**
 * @file uniform.c
 * @brief Provide functions to generate an uniform distribution.
 */

/**
 * @brief Generate a random uniformly distributed number between A and B.
 *
 * @param a Lower bound of the distribution
 * @param b Upper bound of the distribution
 *
 * @return Random number uniformally distributed between A and B.
 */
double uniform(double a, double b)
{
    return a + (b - a) * genrand_real1();
}