#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "mt.h"

/**
 * @file negexp.c
 * @brief Provide functions to reproduct continuous distribution.
 */

/**
 * @brief Reproduct a negative exponential distribution
 *
 * @param mean Mean of the distribution
 *
 * @return A value that follow a negative exponential distribution
 */
double negexp(double mean)
{
    return -1 * mean * log(1 - genrand_real1());
}

/**
 * @brief Compute the average of a reproducted negative exponential distribution
 *
 * @param mean Mean of the distribution
 * @param draw_num Number of time to simulate the distribution
 *
 * @return Average of the negative exponential distribution
 */
double average_negexp(double mean, int draw_num)
{
    double sum = 0;
    int i;
    for (i = 0; i < draw_num; i++)
    {
        sum += negexp(mean);
    }

    return sum / draw_num;
}

/**
 * @brief Compute an histogram of the possible value given by a reproducted negative exponential distribution
 *
 * @param mean Mean of the distribution
 * @param draw_num Number of time to simulate the distribution
 */
void hist_negexp(double mean, int draw_num)
{
    int hist[21] = {0};

    int i;
    int value;
    for (i = 0; i < draw_num; i++)
    {
        value = (int)negexp(mean);
        hist[value <= 20 ? value : 20]++;
    }

    // FILE *f = fopen("negxp.csv", "w");
    for (i = 0; i < 21; i++)
    {
        printf("%d : %d / %d\n", i, hist[i], draw_num);
        // fprintf(f, "%d, %d\n", i, hist[i]);
    }

    // fclose(f);
}