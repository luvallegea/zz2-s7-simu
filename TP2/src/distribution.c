#include <stdlib.h>
#include <stdio.h>

#include "distribution.h"
#include "mt.h"

/**
 * @file distribution.c
 * @brief Provide functions to simulate a distribution.
 */

/**
 * @brief Find the class a given number belongs to.
 *
 * @param number Number to find class
 * @param cum_freqs Cumulative frequencies of the classes
 * @param size Number of classes
 *
 * @return Class of the number
 */
int find_class(double number, double cum_freqs[], int size)
{
    int i = 0;
    while (number > cum_freqs[i] && i < size)
    {
        i++;
    }

    return i;
}

/**
 * @brief Compute the sum of a 1D array
 *
 * @param tab Array to compute sum
 * @param size Number of element in the array
 *
 * @return Sum of every element of the array
 */
int sum(int tab[], int size)
{
    int s = 0;
    int i;
    for (i = 0; i < size; i++)
    {
        s += tab[i];
    }

    return s;
}

/**
 * @brief Reproduct a specific distribution.
 *
 * @param draw_num Number of random draw to do
 */
void distribution_classes(int draw_num)
{
    // int classes[3] = {350, 450, 200};
    // int number = 1000;
    // double frequencies[3] = {0.35, 0.45, 0.2};

    double cumul_freqs[3] = {0.35, 0.8, 1};

    int i;
    int class_draw[3] = {0};

    double random;
    int class;
    for (i = 0; i < draw_num; i++)
    {
        random = genrand_real1();
        class = find_class(random, cumul_freqs, 3);
        class_draw[class]++;
    }

    for (i = 0; i < 3; i++)
    {
        printf("Class %c : %d/%d - %.3f\n", 'A' + i, class_draw[i], draw_num, (double)class_draw[i] / draw_num);
    }
}

/**
 * @brief Reproducts a discrete empirical distribution with n classes.
 *
 * @param classes_number Number of classes
 * @param classes Number of element in each class
 * @param draw_num Number of time to draw for reproduction
 */
void generic_distribution_classes(int classes_number, int classes[], int draw_num)
{
    int cumul = sum(classes, classes_number);

    int i;
    double *freqs = (double *)malloc(classes_number * sizeof(double));
    if (NULL == freqs)
    {
        fprintf(stderr, "[generic_distribution_classes] Couldn't allocate memory for freqs");
        exit(EXIT_FAILURE);
    }
    for (i = 0; i < classes_number; ++i)
    {
        freqs[i] = (double)classes[i] / cumul;
    }

    double *cum_freqs = (double *)malloc(classes_number * sizeof(double));
    if (NULL == cum_freqs)
    {
        fprintf(stderr, "[generic_distribution_classes] Couldn't allocate memory for cum_freqs");
        exit(EXIT_FAILURE);
    }

    double cum_sum = 0;
    for (i = 0; i < classes_number; i++)
    {
        cum_sum += freqs[i];
        cum_freqs[i] = cum_sum;
    }

    double random;
    int class;
    int *class_draw = (int *)calloc(classes_number, sizeof(int));
    if (NULL == class_draw)
    {
        fprintf(stderr, "[generic_distribution_classes] Couldn't allocate memory for class_draw");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < draw_num; i++)
    {
        random = genrand_real1();
        class = find_class(random, cum_freqs, classes_number);
        class_draw[class]++;
    }

    for (i = 0; i < classes_number; i++)
    {
        printf("Class %d : %d/%d -> %.3f\n", i, class_draw[i], draw_num, (double)class_draw[i] / draw_num);
    }

    free(freqs);
    free(cum_freqs);
    free(class_draw);

    freqs = NULL;
    cum_freqs = NULL;
    class_draw = NULL;
}