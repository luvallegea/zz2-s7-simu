#ifndef _NEGEXP_H
#define _NEGEXP_H

double negexp(double mean);
double average_negexp(double mean, int draw_num);
int *hist_negexp(double mean, int draw_num);

#endif