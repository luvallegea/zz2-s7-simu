#ifndef _NON_REVERSIBLE
#define _NON_REVERSIBLE

#define PI 3.14159265

int sum_dices(int draw_number);
void distribution_sum_dices(int draw_num);

void box_muller(int draw_num);

#endif