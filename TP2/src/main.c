#include <stdio.h>
#include <stdlib.h>

#include "mt.h"
#include "uniform.h"
#include "distribution.h"
#include "negexp.h"
#include "non_reversible.h"

int main(int argc, char const *argv[])
{
    (void)argc;
    (void)argv;

    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    int i;

    // Test of Makoto's functions
    printf("1000 outputs of genrand_int32()\n");
    for (i = 0; i < 1000; i++)
    {
        printf("%10lu ", genrand_int32());
        if (i % 5 == 4)
            printf("\n");
    }

    printf("\n1000 outputs of genrand_real1()\n");
    for (i = 0; i < 1000; i++)
    {
        printf("%10.8f ", genrand_real1());
        if (i % 5 == 4)
            printf("\n");
    }

    // Uniform distribution
    printf("\n1000 outputs of uniform()\n");
    for (i = 0; i < 1000; i++)
    {
        printf("%.1lf ", uniform(-98, 57.7));
        if (i % 5 == 4)
            printf("\n");
    }

    // Classes
    printf("\nReproduction of the given classes\n");
    distribution_classes(1000000);

    printf("\nReproduction of genereic classes\n");
    int classes[6] = {100, 400, 600, 400, 100, 200};
    generic_distribution_classes(6, classes, 1000);

    // Continuous distributions
    int draw_cout = 1000000;
    double exp_mean = 10;
    printf("\nReproduction of the negative exponential distribution\n");
    double negexp_test = average_negexp(exp_mean, draw_cout);
    printf("%d draws with theorical mean of %lf: computed mean of %lf\n", draw_cout, exp_mean, negexp_test);

    printf("\nHistogram of the values of the negative exponential distribution\n");
    hist_negexp(10, draw_cout);

    // Non reversible distributions
    printf("\nSimulation of a gaussian function with summation of dice rolls\n");
    distribution_sum_dices(1000000);

    printf("\nSimulation of a gaussian using the Box-Muller method\n");
    box_muller(1000000);

    return 0;
}
