#include <stdio.h>
#include <math.h>

#include "non_reversible.h"
#include "mt.h"

/**
 * @file non_reversible.c
 * @brief Simulation of non reversible distributions
 */

/**
 * @brief Rolls a dice multiple times and sum the result of each roll
 *
 * @param draw_num Number of time to roll the dice

 * @return Sum of the results
 */
int sum_dices(int draw_num)
{
    int random;
    int sum = 0;

    int i = 0;
    for (i = 0; i < draw_num; i++)
    {
        random = (genrand_real1() * 5) + 1;
        sum += random;
    }

    return sum;
}

/**
 * @brief Compute the distribution of the sum of dices using sum_dices
 *
 * @param draw_num Number of time to run sum_dices
 */
void distribution_sum_dices(int draw_num)
{
    int bins[241] = {0};
    int i;
    for (i = 0; i < draw_num; ++i)
    {
        bins[sum_dices(40)]++;
    }

    // FILE *f = fopen("non_reversible_hist.csv", "w");
    for (i = 40; i <= 240; i++)
    {
        printf("%d : %d / %d -> %.2lf\n", i, bins[i], draw_num, (double)bins[i] / draw_num);
        // fprintf(f, "%d, %d\n", i, bins[i]);
    }

    // fclose(f);
}

/**
 * @brief Convert a random number between [-5, 5] to a bin index between [0, 20];
 *
 * @param x Random number
 *
 * @return Index of the bin
 */
int random_to_bin(double x)
{
    return (x + 5) * 2;
}

/**
 * @brief Simulate a gaussian distribution using the Box-Muller method
 *
 * @param draw_num Number of points to draw
 */
void box_muller(int draw_num)
{
    int bins[20] = {0};

    int i;
    double rn1, rn2, x1, x2;
    for (i = 0; i < draw_num; i++)
    {
        rn1 = genrand_real1();
        rn2 = genrand_real1();

        x1 = cos(2 * PI * rn2) * sqrt(-2 * log(rn1));
        x2 = sin(2 * PI * rn2) * sqrt(-2 * log(rn1));

        bins[random_to_bin(x1)]++;
        bins[random_to_bin(x2)]++;
    }

    // FILE *f = fopen("non_reversible_bm.csv", "w");
    for (i = 0; i < 20; i++)
    {
        printf("%.1f: %d / %d -> %.2f\n", ((double)i / 2) - 5, bins[i], draw_num * 2, (double)bins[i] / (draw_num * 2));
        // fprintf(f, "%.1f, %d\n", ((double)i / 2) - 5, bins[i]);
    }

    // fclose(f);
}