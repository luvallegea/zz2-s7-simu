#ifndef _DISTRIBUTION_H
#define _DISTRIBUTION_H

int find_class(double random, double cum_freqs[], int size);
int sum(int tab[], int size);

void distribution_classes(int draw_num);
void generic_distribution_classes(int classes_number, int classes[], int draw_num);

#endif