#include <stdio.h>

/**
 * @brief Generate a random number using linear congruential generators between 0 and m.
 *
 * @param u_n Previous number / seed
 * @param a Slope of the linear function
 * @param c Offset of the linear function
 * @param m Mod value
 * @return Next random value
 */
int linear_random_int(int u_n, int a, int c, int m)
{
    return (a * u_n + c) % m;
}

/**
 * @brief Generate a random number using linear congruential generators between 0 and 1.
 *
 * @param u_n Previous number / seed
 * @param a Slope of the linear function
 * @param c Offset of the linear function
 * @param m Mod value
 * @return Next random value
 */
double linear_random_float(double u_n, int a, int c, int m)
{
    // received value is between 0 and 1 but linear_random_int takes value between 0 and m so we need to multiply and
    // cast to int
    return linear_random_int((int)(u_n * m), a, c, m) / (double)m;
}

int main(int argc, char const *argv[])
{
    (void)argc;
    (void)argv;

    int i;
    double n = 658 / 1024;

    for (i = 0; i < 100; ++i)
    {
        n = linear_random_float(n, 785, 584, 1024);
        printf("%lf\n", n);
    }

    return 0;
}

// To generated scientific RNG, we can use GSL (Gnu Scientigic library)
// To test the distribution of a RNG, we can use TestU01
