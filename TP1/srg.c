#include <stdio.h>
#include <math.h>

#define N_BIT 4

/**
 * @brief Get the nth bit of an int
 *
 * @param value Number to get bit from
 * @param n Bit position, 0 based, reading from right to left (least-significant first)
 * @return nth bit of the value, shifted to 0/1
 */
int get_nth_bit(int value, int pos)

{
    int bit_n = value & (1 << pos);
    int bit_shifted = bit_n >> pos;

    return bit_shifted;
}

/**
 * @brief Generate a random number using shift register generators with prime polynomial (Tausworthe
 * generators)
 *
 * @param p Characteristic polynomial used in generator
 * @param value Previous value / seed
 * @return Random number between 0 and 2^N_BIT - 1
 */
int srg(int p[N_BIT], int value)
{
    int i;
    int bit_n;
    int in_xor = -1; // xor "register"

    for (i = N_BIT - 1; i >= 0; i--)
    {
        if (p[i] == 1)
        {
            bit_n = get_nth_bit(value, N_BIT - i - 1);
            if (in_xor == -1)
            {
                in_xor = bit_n; // register is empty, filling it with current value
            }
            else
            {
                in_xor ^= bit_n; // register is not empty, xor stored value with current value
            }
        }
    }

    value = (value >> 1);                    // shifting on bit
    value = value | (in_xor << (N_BIT - 1)); // adding result of the xor to the shifted value

    return value;
}

int main(int argc, char const *argv[])
{

    (void)argc;
    (void)argv;

    int characteristic_polynomial[N_BIT] = {0, 0, 1, 1}; // x4 + x + 1;
    int value = 0b0110;

    for (int i = 0; i < 32; ++i)
    {
        value = srg(characteristic_polynomial, value);
        printf("%d\n", value);
    }

    return 0;
}
