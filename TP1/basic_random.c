#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Roll a coin-flip n times and prints the distribution. Could consider a coin-flip as a 2 face dice and use
 * random_dice.
 *
 * @param n Number of time to run the coin-flip
 */
void random_coin(int n)
{
    int freq[2] = {0, 0};
    int i;

    for (i = 0; i < n; ++i)
    {
        int r = rand() % 2;
        freq[r]++;
    }

    printf("0: %d %f; 1 : %d %f\n", freq[0], (float)freq[0] / n, freq[1],
           (float)freq[1] / n);
}

/**
 * @brief Roll a dice n times and prints the distribution
 *
 * @param n Number of time to roll dice
 * @param face_number Number of faces of the dice
 */
void random_dice(int n, int face_number)
{
    int *freq = (int *)calloc(face_number * sizeof(int), sizeof(int));
    int i;

    for (i = 0; i < n; ++i)
    {
        int r = rand() % face_number;
        freq[r]++;
    }

    for (i = 0; i < face_number; ++i)
    {
        printf("%d: %d - %f\n", i, freq[i], ((float)freq[i] / n) * 100);
    }

    free(freq);
}

int main(int argc, char const *argv[])
{

    (void)argc;
    (void)argv;

    // random_coin(1000);
    random_dice(1000000, 10);

    return 0;
}
