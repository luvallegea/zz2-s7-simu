#include <stdio.h>

int main(int argc, char const *argv[])
{
    (void)argc;
    (void)argv;

    int u0 = 1234;
    int n = 60;

    int i;
    for (i = 0; i < n; ++i)
    {
        printf("n(%d) : %04d\n", i, u0);
        u0 = (u0 * u0);
        u0 = u0 / 100;   // removing the last 2 digits
        u0 = u0 % 10000; // removing the first 2 digits
    }

    return 0;
};

// after 55 iterations, with seed 1234 it converges to 0
// rand is not suitable for scientific usage