#ifndef PI_H_
#define PI_H_

double simu_pi(int draw_num);
double average_simu_pi(int run_num, int draw_num);

void compute_precision(double simulated_pi);

void confidence_radius(int run_num, int draw_num, double *r, double *m);

#endif