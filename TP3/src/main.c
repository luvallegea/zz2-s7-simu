#include <stdio.h>
#include <stdlib.h>

#include "mt.h"
#include "pi.h"

int main(int argc, char const *argv[])
{
    (void)argc;
    (void)argv;

    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    printf("Simulation de pi sur 1000 tirages : %lf\n", simu_pi(1000));
    printf("Simulation de pi sur 1 000 000 tirages : %lf\n", simu_pi(1000000));
    // printf("Simulation de pi sur 1 000 000 000 tirages : %lf\n", simu_pi(1000000000));

    printf("\nSimulation de pi sur 20 * 1000 tirages : %lf\n", average_simu_pi(20, 1000));
    printf("Simulation de pi sur 20 * 1 000 000 tirages : %lf\n", average_simu_pi(20, 1000000));
    // printf("Simulation de pi sur 20 * 1 000 000 000 tirages : %lf\n", average_simu_pi(20, 1000000000));

    printf("\nPrecision sur 1 * 1000 tirages, pi = 3.124\n");
    compute_precision(3.124000);
    printf("Precision sur 20 * 1 000 000 000 tirages, pi = 3.141583\n");
    compute_precision(3.141583);

    double avg_pi = average_simu_pi(20, 1000000);
    printf("\nComputed value : %lf\n", avg_pi);
    compute_precision(avg_pi);

    double r, m;
    confidence_radius(20, 1000000, &r, &m);
    printf("\nAverage value of pi : %lf\n", m);
    printf("Confidence radius : %lf\n", r);
    printf("Confidence interval : [%lf - %lf]\n", m - r, m + r);

    return 0;
}
