#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mt.h"

static double t_values[30] = {
    12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.308, 2.262, 2.228, 2.201, 2.179,
    2.160, 2.145, 2.131, 2.120, 2.110, 2.101, 2.093, 2.086, 2.080, 2.074, 2.069, 2.064,
    2.060, 2.056, 2.052, 2.048, 2.045, 2.042};

/**
 * @brief Approximate the value of PI by computing quarter of the area of the unit circle
 *
 * @param draw_num Number of points to use to approximate area
 *
 * @return Approximation of PI
 */
double simu_pi(int draw_num)
{
    double x, y;
    int points_in = 0;

    int i;
    for (i = 0; i < draw_num; i++)
    {
        x = genrand_real1();
        y = genrand_real1();

        points_in += ((pow(x, 2) + pow(y, 2)) < 1);
    }

    return ((double)points_in / draw_num) * 4;
}

/**
 * @brief Compute an approximation of PI using multiple replicas to gain precision
 *
 * @param run_num Number of replicas to run
 * @param draw_num Number of point to draw per replica
 *
 * @return Approximation of PI
 */
double average_simu_pi(int run_num, int draw_num)
{
    int i;
    double sum = 0;
    for (i = 0; i < run_num; i++)
    {
        sum += simu_pi(draw_num);
    }

    return sum / run_num;
}

/**
 * @brief Compute the absolute and relative error of a simulated value of PI, requires M_PI from <math.h>
 *
 * @param simulated_pi Simulated value of PI
 */
void compute_precision(double simulated_pi)
{
    double diff = fabs(simulated_pi - M_PI);

    printf("Absolute difference : %lf\n", diff);
    printf("Relative error (%%) : %lf\n", (diff / M_PI) * 100);
}

/**
 * @brief Compute the confidence radius of a simulation of PI
 *
 * @param run_num Number of replicas to run, must be <= 30, if not, function returns and doesn't update r and m.
 * @param draw_num Number of point to draw per replica
 * @param r Confidence radius at 95%, updated by this function
 * @param m Average value of all the simulations of PI, updated by this function
 */
void confidence_radius(int run_num, int draw_num, double *r, double *m)
{
    if (run_num > 30)
    {
        fprintf(stderr, "Cannot compute confidence interval for run_num > 30\n");
        return;
    }

    int i;
    double *pi_values = (double *)malloc(run_num * sizeof(double));

    double sum = 0;
    double pi_value;
    for (i = 0; i < run_num; i++)
    {
        pi_value = simu_pi(draw_num);
        pi_values[i] = pi_value;
        sum += pi_value;
    }

    *m = sum / run_num;
    double s2_n = 0;
    for (i = 0; i < run_num; i++)
    {
        s2_n += pow(pi_values[i] - *m, 2);
    }
    s2_n = s2_n / (draw_num - 1);

    *r = t_values[run_num - 1] * sqrt(s2_n / run_num);

    free(pi_values);
    pi_values = NULL;
}