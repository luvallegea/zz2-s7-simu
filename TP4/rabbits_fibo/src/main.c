#include <stdio.h>

#include "fibo.h"

int main(int argc, char const *argv[]) {
    (void)argc;
    (void)argv;

    printf("%d\n", rabbit_fibo(12));
    return 0;
}
