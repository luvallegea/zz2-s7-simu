#include "fibo.h"

/**
 * @brief Simulate the growth of a rabbit population using the fibonacci sequence.
 *
 * @param generation Number of the generation to simulate

 * @return Number of rabbit in the simulated generation
 */
int rabbit_fibo(int generation)
{
    int a = 0;
    int b = 1;

    // generation 0 doesn't exist in this context
    if (generation <= 1)
    {
        return 1;
    }

    int n = 1;
    int temp;
    while (n < generation)
    {
        temp = a + b;
        a = b;
        b = temp;
        n++;
    }

    return b;
};
