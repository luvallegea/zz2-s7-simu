#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <random>

#include "rabbit.hpp"

class Random {
  private:
    std::mt19937_64 mt;
    std::normal_distribution<double> normal;
    std::uniform_real_distribution<double> uniform;

    LitterParameter &lp;
    KittenParameter &kp;
    float gender_rate;

  public:
    Random(LitterParameter &litter_parameter, KittenParameter &kitten_parameter, float male_rate);

    int litter_number();
    int kittens_number();
    RabbitGender kitten_gender();
    bool rabbit_death(int age);
};

#endif