#ifndef SIMULATE_HPP
#define SIMULATE_HPP

#include "random.hpp"

int simulate_rabbit(Random &rand, int year);

#endif