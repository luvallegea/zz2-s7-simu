#include <map>

#include "rabbit.hpp"

/**
 * @brief Compute the survival rate of a rabbit based on his age
 *
 * @param age Age of the rabbit
 *
 * @return Survival rate of the rabbit
 */
float Rabbit::survivalRate(int age) {
    if (age < 1) {
        return 0.35;
    }
    if (age <= 10) {
        return 0.6;
    }

    return 0.6 - (0.1 * (age - 10));
}

/**
 * @brief Generate a class ID for the rabbit
 *
 * @param age Age of the rabbit
 * @param gender Gender of the rabbit
 *
 * @return ID of the class
 */
int Rabbit::generateId(int age, RabbitGender gender) {
    return (age << 1) + static_cast<int>(gender);
};

/**
 * @brief Get the age of a rabbit from his class ID
 *
 * @param id ID of the class
 *
 * @return Age of the rabbit
 */
int Rabbit::ageFromId(int id) {
    return id >> 1;
};

/**
 * @brief Get the gender of a rabbit from his class ID
 *
 * @param id ID of the class
 *
 * @return Gender of the rabbit
 */
RabbitGender Rabbit::genderFromId(int id) {
    return static_cast<RabbitGender>(id & 1);
};

/**
 * @brief Get the number of rabbits that matches criterias
 *
 * @param rabbits Rabbit classes
 * @param gender Gender of the rabbit
 * @param age Minimum age of the rabbit (inclusive)
 *
 * @return Number of rabbits
 */
long Rabbit::getNumberOf(std::map<int, long> rabbits, RabbitGender gender, int age) {
    long num = 0;
    for (auto rabbit = rabbits.begin(); rabbit != rabbits.end(); rabbit++) {
        if ((Rabbit::ageFromId(rabbit->first) >= age) && (Rabbit::genderFromId(rabbit->first) == gender)) {
            num += rabbit->second;
        }
    }

    return num;
};