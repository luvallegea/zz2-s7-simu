#ifndef RABBIT_HPP
#define RABBIT_HPP

#include <map>
#include <string>

enum class RabbitGender {
    MALE,
    FEMALE,
};

typedef struct litter_parameter_s {
    int min;
    int max;
    float stddev;
} LitterParameter;

typedef struct kitten_parameter_s {
    int min;
    int max;
} KittenParameter;

class Rabbit {
  public:
    static float survivalRate(int age);

    static int generateId(int age, RabbitGender gender);
    static int ageFromId(int id);
    static RabbitGender genderFromId(int id);
    static long getNumberOf(std::map<int, long> rabbits, RabbitGender gender, int age);
};

#endif