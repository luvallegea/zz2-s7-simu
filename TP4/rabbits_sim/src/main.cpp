#include <algorithm>
#include <iostream>
#include <list>
#include <memory>
#include <random>

#include "rabbit.hpp"
#include "random.hpp"
#include "simulation.hpp"

int const N = 10;
int const year = 10;

int main(int argc, char const *argv[]) {
    (void)argc;
    (void)argv;

    LitterParameter lp = {.min = 3, .max = 9, .stddev = 0.75};
    KittenParameter kp = {.min = 3, .max = 6};
    Random rand = Random(lp, kp, 0.5);

    int sum = 0;

    for (int i = 0; i < N; i++) {
        int out = simulate_rabbit(rand, year);
        sum += out;
        // std::cout << "Nombre de lapins à la fin de la replication " << i << " : " << out << "\n";
    }

    std::cout << "Nombre moyen de lapins : " << (double)sum / N << "\n";

    return 0;
}
