#include "random.hpp"
#include <iostream>

Random::Random(LitterParameter &litter_parameter, KittenParameter &kitten_parameter, float male_rate)
    : lp(litter_parameter), kp(kitten_parameter), gender_rate(male_rate) {
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456};
    std::seed_seq seedSeq(std::begin(init), std::end(init));

    double mean = (this->lp.min + this->lp.max) / 2.0;

    this->mt = std::mt19937_64(seedSeq);
    this->uniform = std::uniform_real_distribution<double>(0.0, 1.0);

    // can overflow from min/max depending on stddev
    this->normal = std::normal_distribution<double>(mean, this->lp.stddev);
}

/**
 * @brief Generate a random number of litters a rabbit can have in a year
 *
 * @return Number of litters
 */
int Random::litter_number() {
    double r = this->normal(this->mt);
    return std::round(r);
}

/**
 * @brief Generate a random number of kittens a litter can have
 *
 * @return Number of kittens
 */
int Random::kittens_number() {
    double r = this->uniform(this->mt);
    return std::round(r * this->kp.min) + (this->kp.max - this->kp.min);
}

/**
 * @brief Generate a random gender of a newborn rabbit
 *
 * @return Gender of the rabbit
 */
RabbitGender Random::kitten_gender() {
    double r = this->uniform(this->mt);
    return (r < this->gender_rate) ? RabbitGender::MALE : RabbitGender::FEMALE;
}

/**
 * @brief Randomly decides if a rabbit dies at the end of the year
 *
 * @param age Age of the rabbit
 *
 * @return If the rabbit died or not
 */
bool Random::rabbit_death(int age) {
    return Rabbit::survivalRate(age) < this->uniform(this->mt);
}
