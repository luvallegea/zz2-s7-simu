#include <iostream>
#include <map>
#include <memory>

#include "rabbit.hpp"
#include "random.hpp"

/**
 * @brief Simulate the evolution of a rabbit population
 *
 * @param rand Random generator
 * @param year Number of year to simulate
 *
 * @return Number of rabbits at the end of the simulation
 */
int simulate_rabbit(Random &rand, int year) {
    std::map<int, long> rabbits;

    rabbits[Rabbit::generateId(1, RabbitGender::MALE)]++;
    rabbits[Rabbit::generateId(1, RabbitGender::FEMALE)]++;

    long total_number = 0;

    for (int current_year = 0; current_year < year; current_year++) {
        long maleAdult = Rabbit::getNumberOf(rabbits, RabbitGender::MALE, 1);
        long femaleAdult = Rabbit::getNumberOf(rabbits, RabbitGender::FEMALE, 1);

        long canBirth = maleAdult >= 1 && femaleAdult >= 1;

        // iterating over every rabbit group
        for (auto rabbit = rabbits.begin(); rabbit != rabbits.end() && canBirth; rabbit++) {
            // cannot give birth to new rabbits
            if ((Rabbit::ageFromId(rabbit->first) < 1) || (Rabbit::genderFromId(rabbit->first) == RabbitGender::MALE)) {
                continue;
            }

            // can give birth, compute litter number and kitten per litter
            for (int i = 0; i < rabbit->second; i++) {
                for (int j = 0; j < rand.litter_number(); j++) {
                    int kitten_number = rand.kittens_number();
                    rabbits[Rabbit::generateId(0, rand.kitten_gender())] += kitten_number;
                }
            }
        }

        for (auto rabbit = rabbits.rbegin(); rabbit != rabbits.rend(); rabbit++) {
            int age = Rabbit::ageFromId(rabbit->first);
            RabbitGender gender = Rabbit::genderFromId(rabbit->first);

            long rabbit_num = rabbit->second;

            rabbit->second = 0;
            rabbits[Rabbit::generateId(age + 1, gender)] = std::round(rabbit_num * Rabbit::survivalRate(age));

            // for (int i = 0; i < rabbit_num; i++) {
            //     rabbit->second--;
            //     rabbits[Rabbit::generateId(age + 1, gender)] += !rand.rabbit_death(age);
            // }
        }

        for (const auto &rabbit : rabbits) {
            total_number += rabbit.second;
        }

        // std::cout << "Année " << current_year << ", Nombre : " << total_number << "\n";
    }

    return total_number;
}